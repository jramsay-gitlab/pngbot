# pngbot

PNG image compression for GitLab merge requests. You need to provide a [Personal access tokens](https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html) with `api` sope for the script to work. It is passed to the GitLab Runner via the `PRIVATE_TOKEN` CI variable. Best practice is to create an extra bot account for that purpose. The account needs at least developer rights for your repository to be able to commit the compressed images back to it.

Example output: [GitLab.com pngbot](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/17599/#note_126712119)

## Installation

Add the following lines to your `.gitlab-ci.yml` file:

```yaml
pngbot-commit:
  image: registry.gitlab.com/jramsay/pngbot:v0.1.0
  stage: build
  script:
    - pngbot
```

To target MERGE BASE add:

```yaml
pngbot-all:
  image: registry.gitlab.com/jramsay/pngbot:v0.1.0
  stage: build
  script:
    - pngbot -t=all

```
