# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.2] - 2019-03-18
### Fixes
- Incorrect units in reports
 
## [0.1.1] - 2019-03-18
### Added
- Support for self hosted instances by [Mike Scott](https://gitlab.com/hlf-mike)
- Basic documentation by [Kai](https://gitlab.com/splitt3r)

### Fixes
- Leakage of GitLab access token if git push fails

## [0.1.0] - 2018-11-30
### Added
- Initial release
